export class Task {
  id?: string;
  title: string;
  description?: string;
  done: boolean;
  number: number;
  index: number;
}
