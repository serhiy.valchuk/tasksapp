import { Component, Input } from '@angular/core';
import { Task } from './../../task';
import { TaskService } from './../../services/task.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent {

  @Input() public tasks: Task[];
  public editState: boolean = false;
  public taskToEdit: Task;

  constructor(private _taskService: TaskService) { }

  deleteTask(task): void {
    this._taskService.deleteTask(task);
  }

  editTask(task): void {
    this.editState = !this.editState;
    this.taskToEdit = task;
  }
  
  updateTask(task): void {
    this._taskService.updateTask(task);
    this.editState = !this.editState;
  }

  updateTaskStatus(task): void {
    task.done = !task.done;
    if (task.done) {
      task.index = -task.number;
    } else {
      task.index = task.number;
    }
    this._taskService.updateTask(task);
  }

  cancelUpdatingTask(): void {
    this.editState = !this.editState;
  }
}
