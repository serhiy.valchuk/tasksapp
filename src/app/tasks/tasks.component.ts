import { Component, OnInit } from '@angular/core';
import { Task } from '../task';
import { TaskService } from '../services/task.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  public activeTasks: Task[];
  public completedTasks: Task[];

  constructor(private _taskService: TaskService) { }

  ngOnInit() {
    this.init();
  }

  init(): void {
    // Get active tasks
    this._taskService.init(
      environment.properties.tasksCollection,
      environment.properties.orderField,
      false,
      environment.properties.limit )
      .subscribe(tasks => { this.activeTasks = tasks });

    // Get done tasks
    this._taskService.init(
      environment.properties.tasksCollection,
      environment.properties.orderField,
      true,
      environment.properties.limit )
      .subscribe(tasks => { this.completedTasks = tasks });
  }

  loadMoreTasks(): void {
    console.log('Click!');
  }
}
