import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { GetPropertiesService } from '../../services/get-properties.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  public addState: boolean = false;
  public currentTasksAmount: number;
  public totalTasksAmount: number;
  public index: number;

  constructor(
    private _taskService: TaskService,
    private _getAppProperty: GetPropertiesService ) { }

  ngOnInit() {
    this.getAppProperties();
  }

  addTask(title, description): void {
    this.addState = !this.addState;
    this.currentTasksAmount++;
    this.totalTasksAmount++;
    this.index = this.totalTasksAmount;
    this._taskService.addTask(title, description, this.totalTasksAmount, this.index);
    this._getAppProperty.updateAppProperty(environment.properties.currentTasksAmount, this.currentTasksAmount);
    this._getAppProperty.updateAppProperty(environment.properties.totalTasksAmount, this.totalTasksAmount);
  }

  toggleAddForm(): void {
    this.addState = !this.addState;
  }

  getAppProperties(): void {
    this._getAppProperty.getAppProperty(environment.properties.currentTasksAmount)
      .subscribe(doc => {
        this.currentTasksAmount = doc.amount;
      });
    this._getAppProperty.getAppProperty(environment.properties.totalTasksAmount)
      .subscribe(doc => {
        this.totalTasksAmount = doc.amount;
      })
  }

}
