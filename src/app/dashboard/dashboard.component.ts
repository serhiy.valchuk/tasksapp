import { Component, OnInit } from '@angular/core';
import { Task } from './../task';
import { TaskService } from './../services/task.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public tasks: Task[];

  constructor(private _taskService: TaskService) { }

  ngOnInit() {
    this.init();
  }

  init(): void {
    // Get active tasks
    this._taskService.init(
      environment.properties.tasksCollection,
      environment.properties.orderField,
      false,
      3)
      .subscribe(tasks => { this.tasks = tasks });
    }

}
