import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class GetPropertiesService {
  private _taskProperties: AngularFirestoreCollection<any>;

  constructor(private _afs: AngularFirestore) {
    this._taskProperties = _afs.collection<any>('properties');
  }

  getAppProperty(property: string): Observable<any> {
    return this._taskProperties.doc(property).valueChanges();
  }

  updateAppProperty(property: string, number: number): void {
    this._taskProperties.doc(property).update({amount: number});
  }
}
