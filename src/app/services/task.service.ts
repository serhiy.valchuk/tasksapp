import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';

import { Task } from '../task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private _tasksCollection: AngularFirestoreCollection<Task>;
  private _data: BehaviorSubject<Task[]> = new BehaviorSubject([]);

  public data: Observable<Task[]>;
  public latestEntry: any;
  public task: AngularFirestoreDocument<Task>;


  constructor(private _afs: AngularFirestore) {
    this._tasksCollection = _afs.collection<Task>('tasks');
  }

  getTasks(collection: AngularFirestoreCollection<Task>): Observable<Task[]> {
    return collection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Task;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  init(collection: string, field: string, done: boolean, limit=Infinity): Observable<Task[]> {
    const first: AngularFirestoreCollection<Task> = this._afs.collection(collection, ref => {
      return ref
              .where(field, done ? '<' : '>', 0)
              .orderBy(field, done ? 'asc' : 'desc')
              .limit(limit);
    });

    return this.getTasks(first);
  }

  addTask(title: string, description: string, number: number, index: number): void {
    this._tasksCollection.add({
      title: title,
      description: description,
      done: false,
      number: number,
      index: index
    });
  }

  deleteTask(task): void {
    this.task = this._afs.doc(`tasks/${task.id}`);
    this.task.delete();
  }

  updateTask(task): void {
    this.task = this._afs.doc(`tasks/${task.id}`);
    this.task.update(task);
  }

}
