// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC5jCuQ2YU3JqpjU8C9VsY6hceLZM0vbDI",
    authDomain: "tasks-8fdd5.firebaseapp.com",
    databaseURL: "https://tasks-8fdd5.firebaseio.com",
    projectId: "tasks-8fdd5",
    storageBucket: "tasks-8fdd5.appspot.com",
    messagingSenderId: "65583519664"
  },
  properties: {
    tasksCollection: 'tasks',
    propertiesCollection: 'properties',
    currentTasksAmount: 'currentTasksAmount',
    totalTasksAmount: 'totalTasksAmount',
    limit: 20,
    orderField: 'index'
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
