(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tasks_tasks_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tasks/tasks.component */ "./src/app/tasks/tasks.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"] },
    { path: 'tasks', component: _tasks_tasks_component__WEBPACK_IMPORTED_MODULE_2__["TasksComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)
            ],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app {\n  border-top: 1px solid rgba(0,0,0,0.25);\n  border-bottom: 1px solid rgba(0,0,0,0.25);\n  color: #333;\n  display: flex;\n  flex-direction: column;\n  height: 100%;\n  max-width: 425px;\n  margin: auto;\n}\n.appTop {\n  flex-shrink: 0;\n}\n.appMain {\n  flex-grow: 1;\n  overflow-y: auto;\n}\n.appHeader {\n  background-color: rgba(0,0,0,0.05);\n  border: 1px solid rgba(0,0,0,0.25);\n  padding: 15px;\n  display: flex;\n  flex-wrap: wrap;\n  justify-content: space-between;\n}\n.appNav {\n  list-style: none;\n  margin: 0 -5px;\n  padding: 0;\n  display: flex;\n}\n.addTask {\n  display: block;\n  flex-grow: 1;\n  margin-left: 30px;\n  text-align: right;\n}\n.appNavItem {\n  margin: 0 5px;\n}\n.appNavActiveLink {\n  color: #000;\n}\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app\">\n  <div class=\"appTop\">\n    <header class=\"appHeader\">\n      <div class=\"appName\">TaskForce!</div>\n      <app-add class=\"addTask\"></app-add>\n    </header>\n\n    <nav class=\"appSection\">\n      <ul class=\"appNav\">\n        <li class=\"appNavItem\">\n          <a routerLink=\"/dashboard\" routerLinkActive=\"appNavActiveLink\">Dashboard</a>\n        </li>\n        <li class=\"appNavItem\">\n          <a routerLink=\"/tasks\" routerLinkActive=\"appNavActiveLink\">Task Lists</a>\n        </li>\n      </ul>\n    </nav>\n  </div>\n\n  <div class=\"appMain\">\n    <router-outlet></router-outlet>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Tasks Force';
        this.description = 'Tasks, Time Tracker and Reporter App';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var angularfire2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angularfire2 */ "./node_modules/angularfire2/index.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angularfire2/database */ "./node_modules/angularfire2/database/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _tasks_add_add_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./tasks/add/add.component */ "./src/app/tasks/add/add.component.ts");
/* harmony import */ var _tasks_task_list_task_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./tasks/task-list/task-list.component */ "./src/app/tasks/task-list/task-list.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _tasks_tasks_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./tasks/tasks.component */ "./src/app/tasks/tasks.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__["DashboardComponent"],
                _tasks_add_add_component__WEBPACK_IMPORTED_MODULE_9__["AddComponent"],
                _tasks_task_list_task_list_component__WEBPACK_IMPORTED_MODULE_10__["TaskListComponent"],
                _tasks_tasks_component__WEBPACK_IMPORTED_MODULE_12__["TasksComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                angularfire2__WEBPACK_IMPORTED_MODULE_4__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].firebase),
                angularfire2_database__WEBPACK_IMPORTED_MODULE_5__["AngularFireDatabaseModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_11__["AppRoutingModule"]
            ],
            providers: [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestore"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"appDashboard\">\n  <div class=\"appSection\">\n    <h1>Dashboard</h1>\n  </div>\n\n  <div class=\"appSection\">\n    <h2>Last tasks:</h2>\n    <app-task-list [tasks]=tasks></app-task-list>\n    <a routerLink=\"/tasks\">More tasks</a>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../services/task.service */ "./src/app/services/task.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(_taskService) {
        this._taskService = _taskService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.init();
    };
    DashboardComponent.prototype.init = function () {
        var _this = this;
        // Get active tasks
        this._taskService.init(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].properties.tasksCollection, _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].properties.orderField, false, 3)
            .subscribe(function (tasks) { _this.tasks = tasks; });
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_1__["TaskService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/services/get-properties.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/get-properties.service.ts ***!
  \****************************************************/
/*! exports provided: GetPropertiesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetPropertiesService", function() { return GetPropertiesService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GetPropertiesService = /** @class */ (function () {
    function GetPropertiesService(_afs) {
        this._afs = _afs;
        this._taskProperties = _afs.collection('properties');
    }
    GetPropertiesService.prototype.getAppProperty = function (property) {
        return this._taskProperties.doc(property).valueChanges();
    };
    GetPropertiesService.prototype.updateAppProperty = function (property, number) {
        this._taskProperties.doc(property).update({ amount: number });
    };
    GetPropertiesService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], GetPropertiesService);
    return GetPropertiesService;
}());



/***/ }),

/***/ "./src/app/services/task.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/task.service.ts ***!
  \******************************************/
/*! exports provided: TaskService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskService", function() { return TaskService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TaskService = /** @class */ (function () {
    function TaskService(_afs) {
        this._afs = _afs;
        this._data = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"]([]);
        this._tasksCollection = _afs.collection('tasks');
    }
    TaskService.prototype.getTasks = function (collection) {
        return collection.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (actions) { return actions.map(function (a) {
            var data = a.payload.doc.data();
            var id = a.payload.doc.id;
            return __assign({ id: id }, data);
        }); }));
    };
    TaskService.prototype.init = function (collection, field, done, limit) {
        if (limit === void 0) { limit = Infinity; }
        var first = this._afs.collection(collection, function (ref) {
            return ref
                .where(field, done ? '<' : '>', 0)
                .orderBy(field, done ? 'asc' : 'desc')
                .limit(limit);
        });
        return this.getTasks(first);
    };
    TaskService.prototype.addTask = function (title, description, number, index) {
        this._tasksCollection.add({
            title: title,
            description: description,
            done: false,
            number: number,
            index: index
        });
    };
    TaskService.prototype.deleteTask = function (task) {
        this.task = this._afs.doc("tasks/" + task.id);
        this.task.delete();
    };
    TaskService.prototype.updateTask = function (task) {
        this.task = this._afs.doc("tasks/" + task.id);
        this.task.update(task);
    };
    TaskService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]])
    ], TaskService);
    return TaskService;
}());



/***/ }),

/***/ "./src/app/tasks/add/add.component.css":
/*!*********************************************!*\
  !*** ./src/app/tasks/add/add.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".totalTasks {\n  margin-top: 10px;\n}\n.addTask {\n  background-color: #f2f2f2;\n  border: 1px solid rgba(0,0,0,0.25);\n  padding: 15px;\n  width: 425px;\n  -webkit-transform: translateX(-50%);\n          transform: translateX(-50%);\n  max-width: 100%;\n  text-align: left;\n  position: fixed;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 50%;\n  z-index: 101;\n  opacity: 0;\n  visibility: hidden;\n  transition: all 0.3s;\n}\n.addTaskVisible {\n  opacity: 1;\n  visibility: visible;\n}\n"

/***/ }),

/***/ "./src/app/tasks/add/add.component.html":
/*!**********************************************!*\
  !*** ./src/app/tasks/add/add.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button (click)=\"toggleAddForm()\">add new task</button>\n<div class=\"totalTasks\">Total tasks were added: {{ totalTasksAmount }}</div>\n<div class=\"addTask\" [class.addTaskVisible]=\"addState\">\n  <form (ngSubmit)=\"addTask(taskTitle.value, taskDescription.value)\" *ngIf=\"addState\">\n    <fieldset class=\"addTaskFieldset\">\n      <legend class=\"addTaskLegend\">add new task</legend>\n      <div class=\"formField\">\n        <label class=\"addTaskLabel\" for=\"task-title\">task title</label>\n        <input class=\"addTaskControl\" #taskTitle type=\"text\" name=\"task-title\" id=\"task-title\"\n                placeholder=\"task title\" autofocus>\n      </div>\n      <div class=\"formField\">\n        <label class=\"addTaskLabel\" for=\"task-description\">task description</label>\n        <input class=\"addTaskControl\" #taskDescription type=\"text\"\n                name=\"task-description\" id=\"task-description\"\n                placeholder=\"task description\">\n      </div>\n    </fieldset>\n    <button>add</button>\n    <button type=\"button\" (click)=\"toggleAddForm()\">cancel</button>\n  </form>\n</div>\n"

/***/ }),

/***/ "./src/app/tasks/add/add.component.ts":
/*!********************************************!*\
  !*** ./src/app/tasks/add/add.component.ts ***!
  \********************************************/
/*! exports provided: AddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddComponent", function() { return AddComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/task.service */ "./src/app/services/task.service.ts");
/* harmony import */ var _services_get_properties_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/get-properties.service */ "./src/app/services/get-properties.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = /** @class */ (function () {
    function AddComponent(_taskService, _getAppProperty) {
        this._taskService = _taskService;
        this._getAppProperty = _getAppProperty;
        this.addState = false;
    }
    AddComponent.prototype.ngOnInit = function () {
        this.getAppProperties();
    };
    AddComponent.prototype.addTask = function (title, description) {
        this.addState = !this.addState;
        this.currentTasksAmount++;
        this.totalTasksAmount++;
        this.index = this.totalTasksAmount;
        this._taskService.addTask(title, description, this.totalTasksAmount, this.index);
        this._getAppProperty.updateAppProperty(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].properties.currentTasksAmount, this.currentTasksAmount);
        this._getAppProperty.updateAppProperty(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].properties.totalTasksAmount, this.totalTasksAmount);
    };
    AddComponent.prototype.toggleAddForm = function () {
        this.addState = !this.addState;
    };
    AddComponent.prototype.getAppProperties = function () {
        var _this = this;
        this._getAppProperty.getAppProperty(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].properties.currentTasksAmount)
            .subscribe(function (doc) {
            _this.currentTasksAmount = doc.amount;
        });
        this._getAppProperty.getAppProperty(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].properties.totalTasksAmount)
            .subscribe(function (doc) {
            _this.totalTasksAmount = doc.amount;
        });
    };
    AddComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add',
            template: __webpack_require__(/*! ./add.component.html */ "./src/app/tasks/add/add.component.html"),
            styles: [__webpack_require__(/*! ./add.component.css */ "./src/app/tasks/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_1__["TaskService"],
            _services_get_properties_service__WEBPACK_IMPORTED_MODULE_2__["GetPropertiesService"]])
    ], AddComponent);
    return AddComponent;
}());



/***/ }),

/***/ "./src/app/tasks/task-list/task-list.component.css":
/*!*********************************************************!*\
  !*** ./src/app/tasks/task-list/task-list.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".taskList {\n  list-style: none;\n  margin: 10px 0;\n  padding: 0;\n}\n.taskItem {\n  margin-bottom: 10px;\n}\n.taskItem--done {\n  color: #ccc;\n}\n.taskItemRow {\n  display: flex;\n}\n.taskItemContent {\n  flex-grow: 1;\n}\n.taskItemActions {\n  flex-shrink: 0;\n}\n.taskItemEdit {\n  margin-top: 5px;\n}\n.loading {\n  margin: 10px 0;\n}\n"

/***/ }),

/***/ "./src/app/tasks/task-list/task-list.component.html":
/*!**********************************************************!*\
  !*** ./src/app/tasks/task-list/task-list.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"taskList\" *ngIf=\"tasks; else loading\">\n  <li class=\"taskItem\"\n      [class.taskItem--done]=\"task.done\"\n      *ngFor=\"let task of tasks\">\n    <div class=\"taskItemRow\">\n      <div class=\"taskItemContent\">\n        <h3 class=\"taskItemTitle\">\n          <input class=\"taskItemStatus\" type=\"checkbox\"\n                 [checked]=\"task.done\"\n                 (change)=\"updateTaskStatus(task)\"\n                 *ngIf=\"!editState\">\n          #{{task.number}}. {{task.title}}\n        </h3>\n        <div class=\"taskItemDescription\">{{task.description}}</div>\n      </div>\n      <div class=\"taskItemActions\">\n        <button type=\"button\" (click)=\"editTask(task)\"\n                *ngIf=\"!editState && !task.done\">edit</button>\n        <button type=\"button\" (click)=\"deleteTask(task)\">delete</button>\n      </div>\n    </div>\n    <div class=\"taskItemEdit\" *ngIf=\"editState && taskToEdit.id === task.id\">\n      <form (ngSubmit)=\"updateTask(task)\">\n          <div class=\"formField\">\n            <input type=\"text\" name=\"title\"\n              placeholder=\"edit title\" [(ngModel)]=\"task.title\">\n          </div>\n          <div class=\"formField\">\n              <input type=\"text\" name=\"description\"\n                placeholder=\"edit description\" [(ngModel)]=\"task.description\">\n          </div>\n          <button>update task</button>\n          <button type=\"button\" (click)=\"cancelUpdatingTask()\">cancel</button>\n      </form>\n    </div>\n  </li>\n</ul>\n\n<ng-template #loading>\n  <div class=\"loading\">loading...</div>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/tasks/task-list/task-list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/tasks/task-list/task-list.component.ts ***!
  \********************************************************/
/*! exports provided: TaskListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaskListComponent", function() { return TaskListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/task.service */ "./src/app/services/task.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TaskListComponent = /** @class */ (function () {
    function TaskListComponent(_taskService) {
        this._taskService = _taskService;
        this.editState = false;
    }
    TaskListComponent.prototype.deleteTask = function (task) {
        this._taskService.deleteTask(task);
    };
    TaskListComponent.prototype.editTask = function (task) {
        this.editState = !this.editState;
        this.taskToEdit = task;
    };
    TaskListComponent.prototype.updateTask = function (task) {
        this._taskService.updateTask(task);
        this.editState = !this.editState;
    };
    TaskListComponent.prototype.updateTaskStatus = function (task) {
        task.done = !task.done;
        if (task.done) {
            task.index = -task.number;
        }
        else {
            task.index = task.number;
        }
        this._taskService.updateTask(task);
    };
    TaskListComponent.prototype.cancelUpdatingTask = function () {
        this.editState = !this.editState;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Array)
    ], TaskListComponent.prototype, "tasks", void 0);
    TaskListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-task-list',
            template: __webpack_require__(/*! ./task-list.component.html */ "./src/app/tasks/task-list/task-list.component.html"),
            styles: [__webpack_require__(/*! ./task-list.component.css */ "./src/app/tasks/task-list/task-list.component.css")]
        }),
        __metadata("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_1__["TaskService"]])
    ], TaskListComponent);
    return TaskListComponent;
}());



/***/ }),

/***/ "./src/app/tasks/tasks.component.css":
/*!*******************************************!*\
  !*** ./src/app/tasks/tasks.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".actions {\n  margin-bottom: 20px;\n}\n"

/***/ }),

/***/ "./src/app/tasks/tasks.component.html":
/*!********************************************!*\
  !*** ./src/app/tasks/tasks.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"appSection\">\n  <h2>Active tasks:</h2>\n  <app-task-list [tasks]=activeTasks></app-task-list>\n  <!-- <div class=\"actions\">\n    <button type=\"button\" (click)=\"loadMoreTasks()\">Load more tasks</button>\n  </div> -->\n\n  <h2>Completed task:</h2>\n  <app-task-list [tasks]=completedTasks></app-task-list>\n\n</div>\n"

/***/ }),

/***/ "./src/app/tasks/tasks.component.ts":
/*!******************************************!*\
  !*** ./src/app/tasks/tasks.component.ts ***!
  \******************************************/
/*! exports provided: TasksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TasksComponent", function() { return TasksComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_task_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/task.service */ "./src/app/services/task.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TasksComponent = /** @class */ (function () {
    function TasksComponent(_taskService) {
        this._taskService = _taskService;
    }
    TasksComponent.prototype.ngOnInit = function () {
        this.init();
    };
    TasksComponent.prototype.init = function () {
        var _this = this;
        // Get active tasks
        this._taskService.init(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].properties.tasksCollection, _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].properties.orderField, false, _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].properties.limit)
            .subscribe(function (tasks) { _this.activeTasks = tasks; });
        // Get done tasks
        this._taskService.init(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].properties.tasksCollection, _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].properties.orderField, true, _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].properties.limit)
            .subscribe(function (tasks) { _this.completedTasks = tasks; });
    };
    TasksComponent.prototype.loadMoreTasks = function () {
        console.log('Click!');
    };
    TasksComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tasks',
            template: __webpack_require__(/*! ./tasks.component.html */ "./src/app/tasks/tasks.component.html"),
            styles: [__webpack_require__(/*! ./tasks.component.css */ "./src/app/tasks/tasks.component.css")]
        }),
        __metadata("design:paramtypes", [_services_task_service__WEBPACK_IMPORTED_MODULE_1__["TaskService"]])
    ], TasksComponent);
    return TasksComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyC5jCuQ2YU3JqpjU8C9VsY6hceLZM0vbDI",
        authDomain: "tasks-8fdd5.firebaseapp.com",
        databaseURL: "https://tasks-8fdd5.firebaseio.com",
        projectId: "tasks-8fdd5",
        storageBucket: "tasks-8fdd5.appspot.com",
        messagingSenderId: "65583519664"
    },
    properties: {
        tasksCollection: 'tasks',
        propertiesCollection: 'properties',
        currentTasksAmount: 'currentTasksAmount',
        totalTasksAmount: 'totalTasksAmount',
        limit: 20,
        orderField: 'index'
    }
};
/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\develop\taskforce\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map